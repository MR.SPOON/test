clear
#!/bin/bash/
#version1.0
blue='\033[34;1m'
green='\033[32;1m'
purple='\033[35;1m'
cyan='\033[36;1m'
red='\033[31;1m'
white='\033[37;1m'
yellow='\033[33;1m'
echo $red"""**********                   **     **      **  ** 
/////**///                   /**    /**     /** *** 
    /**      *****   ****** ******  /**     /**//** 
    /**     **///** **//// ///**/   //**    **  /** 
    /**    /*******//*****   /**     //**  **   /** 
    /**    /**////  /////**  /**      //****    /** 
    /**    //****** ******   //**      //**     ****
    //      ////// //////     //        //     //// 

           $white+-+ +-+ +-+ +-+ +-+ +-+ +-+   +-+
           |V| |e| |r| |s| |i| |o| |n| - |1|
           +-+ +-+ +-+ +-+ +-+ +-+ +-+   +-+
"""
echo $red"[+]………………………………………………………………………………………………………[+]"
echo $yellow"CREATOR <-> HENDRIK (MR.SPOON)"
echo $yellow"TEAM <-> INDONESIA DEATH CODERS"
echo $yellow"     <-> MADURA DOWN SECURITY"
echo $yellow"GITLAB <-> gitlab.com/MR.SPOON/"
echo $red"[+]…………………………………………………………………………………………………………[+]"
echo
read -p "MASUKAN WEBSITE:" web;
echo
clear
echo $blue"        ------------------------------------------"
echo $red"                List Of Scans Or Actions "
echo $blue"        ------------------------------------------"
echo $cyan"                Scaning : $green $web"
echo
echo
echo $cyan" [1] DNS LOOKUP"
echo $cyan" [2] WHOIS LOOKUP"
echo $cyan" [3] TRACEROUTE"
echo $cyan" [4] REVERSE DNS LOOKUP"
echo $cyan" [5] GEO IP LOOKUP"
echo $cyan" [6] PORT SCAN"
echo $cyan" [7] REVERSE IP LOOKUP"
echo $cyan" [8] NPING"
echo $cyan" [9] HTTP HEADERS CHECK"
echo $cyan" [10] ZONE TRANSFER"
echo $cyan" [11] SUBDOMAIN FINDER"
echo $purple" [12] WEB SALAH!! KEMBALI"
echo $red" [13] EXIT"
echo
read -p "[#] PILIH SALAH SATU:" pil;

if [ $pil = 1 ]
then
echo $yellow"SCANING...."
echo $yellow"----------------------------"
echo $red"    HASIL"
echo $yellow"----------------------------"
curl http://api.hackertarget.com/dnslookup/?q=$web
echo $yellow"----------------------------"
fi

if [ $pil = 2 ]
then
echo $yellow"SCANING...."
echo $yellow"----------------------------"
echo $red"     HASIL"
echo $yellow"----------------------------"
curl http://api.hackertarget.com/whois/?q=$web
echo $yellow"----------------------------"
fi


if [ $pil = 3 ]
then
echo $yellow"SCANING...."
echo $yellow"----------------------------"
echo $red"     HASIL"
echo $yellow"----------------------------"
curl http://api.hackertarget.com/mtr/?q=$web
echo $yellow"----------------------------"
fi

if [ $pil = 4 ]
then
echo $yellow"SCANING...."
echo $yellow"----------------------------"
echo $red"     HASIL"
echo $yellow"----------------------------"
curl http://api.hackertarget.com/reversedns/?q=$web
echo $yellow"----------------------------"
fi

if [ $pil = 5 ]
then
echo $yellow"SCANING...."
echo $yellow"----------------------------"
echo $red"     HASIL"
echo $yellow"----------------------------"
curl http://api.hackertarget.com/geoip/?q=$web
echo $yellow"----------------------------"
fi

if [ $pil = 6 ]
then
echo $yellow"SCANING...."
echo $yellow"----------------------------"
echo $red"     HASIL"
echo $yellow"----------------------------"
curl http://api.hackertarget.com/nmap/?q=$web
echo $yellow"----------------------------"
fi

if [ $pil = 7 ]
then
echo $yellow"SCANING...."
echo $yellow"----------------------------"
echo $red"     HASIL"
echo $yellow"----------------------------"
curl http://api.hackertarget.com/reverseiplookup/?q=$web
echo $yellow"----------------------------"
fi

if [ $pil = 8 ]
then
echo $yellow"SCANING...."
echo $yellow"----------------------------"
echo $red"     HASIL"
echo $yellow"----------------------------"
curl http://api.hackertarget.com/nping/?q=$web
echo $yellow"----------------------------"
fi

if [ $pil = 9 ]
then
echo $yellow"SCANING...."
echo $yellow"----------------------------"
echo $red"     HASIL"
echo $yellow"----------------------------"
curl http://api.hackertarget.com/httpheaders/?q=$web
echo $yellow"----------------------------"
fi

if [ $pil = 10 ]
then
echo $yellow"SCANING...."
echo $yellow"----------------------------"
echo $red"     HASIL"
echo $yellow"----------------------------"
curl http://api.hackertarget.com/zonetransfer/?q=$web
echo $yellow"----------------------------"
fi

if [ $pil = 11 ]
then
echo $yellow"SCANING...."
echo $yellow"----------------------------"
echo $red"     HASIL"
echo $yellow"----------------------------"
curl http://api.hackertarget.com/hostsearch/?q=$web
echo $yellow"----------------------------"
fi

if [ $pil = 12 ] 
then
sh test.sh
fi

if [ $pil = 13 ]
then
exit
fi
